// Design: Bootloader ram/sdspi
// Description: 
// Author: German Cano Quiveu <germancq@dte.us.es>
// Copyright Universidad de Sevilla, Spain
// Rev: 25, aug 2017

module bootloader_module (
  input clk,
  input reset,
  input start,
  output reg o_cpu_rst,
  input i_ram_ack,
  input i_nanofs_busy,
  input i_nanofs_err,
  input i_nanofs_file_not_found,
  input i_nanofs_eof,
  input [7:0] i_nanofs_data,
  output reg o_nanofs_rst,
  output reg o_nanofs_byte,
  output reg o_nanofs_start,
  output reg o_ram_rst,
  output reg o_ram_we,
  output reg o_ram_cyc,
  output reg o_ram_stb,
  output reg o_ram_cti,
  output reg [3:0] o_ram_sel,
  output reg [31:0] o_ram_data,
  output reg [31:0] o_ram_addr,
  output reg o_mux,
  output reg finish_led,
  output reg idle_led,
  output error_file,
  output error);

parameter SDSPI_DATA_WIDTH = 32;

reg [3:0] current_state;
reg [3:0] next_state;

parameter IDLE = 4'h0;
parameter WAIT_INIT = 4'h1;
parameter WAIT_0 = 4'h2;
parameter WAIT_NANOFS = 4'h3;
parameter READ_BYTE = 4'h4;
parameter PRE_LOAD = 4'h5;
parameter LOAD_DATA_0 = 4'h6;
parameter POST_LOAD = 4'h7;
parameter ERROR = 4'h8;
parameter END_FSM = 4'h9;

reg up;
wire [31:0] counter_o;
reg rst_counter;
assign counter_int = counter_o[7];
reg [31:0] address_ram;

contador_up counter0(
   .clk(clk),
   .rst(rst_counter),
   .up(up),
   .q(counter_o)
);


reg rst_counter_word;
wire [31:0] counter_word_o;
reg up_word;
assign counter_word = counter_word_o[2];

contador_up counter2(
   .clk(clk),
   .rst(rst_counter_word),
   .up(up_word),
   .q(counter_word_o)
);




wire [31:0] reg_sdspi_data;
wire [7:0] reg_sdspi_data_0,reg_sdspi_data_1,reg_sdspi_data_2,reg_sdspi_data_3;
assign reg_sdspi_data = {reg_sdspi_data_3,reg_sdspi_data_2,reg_sdspi_data_1,reg_sdspi_data_0};
reg reg_sdspi_data_0_cl;
reg reg_sdspi_data_0_w;
registro r0(
	.clk(clk),
	.cl(reg_sdspi_data_0_cl),
	.w(reg_sdspi_data_0_w),
	.din(i_nanofs_data),
	.dout(reg_sdspi_data_0)
);
reg reg_sdspi_data_1_cl;
reg reg_sdspi_data_1_w;
registro r1(
	.clk(clk),
	.cl(reg_sdspi_data_1_cl),
	.w(reg_sdspi_data_1_w),
	.din(i_nanofs_data),
	.dout(reg_sdspi_data_1)
);
reg reg_sdspi_data_2_cl;
reg reg_sdspi_data_2_w;
registro r2(
	.clk(clk),
	.cl(reg_sdspi_data_2_cl),
	.w(reg_sdspi_data_2_w),
	.din(i_nanofs_data),
	.dout(reg_sdspi_data_2)
);
reg reg_sdspi_data_3_cl;
reg reg_sdspi_data_3_w;
registro r3(
	.clk(clk),
	.cl(reg_sdspi_data_3_cl),
	.w(reg_sdspi_data_3_w),
	.din(i_nanofs_data),
	.dout(reg_sdspi_data_3)
);


wire i_btn_start;
pulse_button pulse1(
   .clk(clk),
   .reset(1'b0),
   .button(start),
   .pulse(i_btn_start)
);

reg [31:0] reg_ram_data;
initial current_state = IDLE;


//Next State
always @(*)
begin
	next_state = current_state;

	case(current_state)
		IDLE:
			begin
				if(i_btn_start == 1'b1)
					next_state = WAIT_INIT;
			end
	  	WAIT_INIT:
			begin
				if(i_nanofs_busy == 1'b0)
					next_state = WAIT_0;
			end
		WAIT_0://estado para asegurar el valor de los counter
			begin
				next_state = WAIT_NANOFS;
			end
		WAIT_NANOFS:
			begin
				if(i_nanofs_busy == 1'b0)
				begin
					if(i_nanofs_eof == 1'b1)
						next_state = END_FSM;
					else if(counter_word)
						next_state = PRE_LOAD;
					else
						next_state = READ_BYTE;
				end
			end
		READ_BYTE:
			begin
				next_state = WAIT_0;
			end
		PRE_LOAD:
			begin
				next_state = LOAD_DATA_0;
			end
		LOAD_DATA_0:
			begin
				if(i_ram_ack == 1'b1)
					next_state = POST_LOAD;
			end
		POST_LOAD:
			begin
				next_state = WAIT_0;
			end
		END_FSM:
			begin
			end
		ERROR:
			begin
			end
		default: next_state = IDLE;
	endcase
end


assign error = i_nanofs_err;
assign error_file = i_nanofs_file_not_found;
//Outputs
always @(*)
begin
	o_mux = 1'b1;

	o_ram_rst = 1'b0;
	o_ram_we = 1'b0;
	o_ram_cyc = 1'b0;
	o_ram_stb = 1'b0;
	o_ram_cti = 1'b0;
	o_ram_sel = 4'b0000;
	o_ram_data = reg_sdspi_data;
   	o_ram_addr = 32'h00000000 |(counter_o<<2);

	o_nanofs_rst = 1'b0;
	o_nanofs_byte = 1'b0;
	o_nanofs_start = 1'b1;

	rst_counter = 1'b0;
	rst_counter_word = 1'b0;
	up = 1'b0;
	up_word = 1'b0;

	reg_sdspi_data_3_w = 0;
	reg_sdspi_data_2_w = 0;
	reg_sdspi_data_1_w = 0;
	reg_sdspi_data_0_w = 0;
	reg_sdspi_data_3_cl = 0;
	reg_sdspi_data_2_cl = 0;
	reg_sdspi_data_1_cl = 0;
	reg_sdspi_data_0_cl = 0;

   	o_cpu_rst = 1'b1;

	idle_led = 1'b0;
	finish_led = 1'b0;

	case(current_state)
		IDLE:
			begin
			    o_mux = 1'b0;
				o_ram_rst = 1'b1;
				o_nanofs_rst = 1'b1;
				o_cpu_rst = 1'b0;
				rst_counter = 1'b1;
				rst_counter_word = 1'b1;

				reg_sdspi_data_3_cl = 1;
				reg_sdspi_data_2_cl = 1;
				reg_sdspi_data_1_cl = 1;
				reg_sdspi_data_0_cl = 1;

				idle_led = 1'b1;		
			end
	  	WAIT_INIT:
			begin
				
			end
		WAIT_0:
			begin

			end
		WAIT_NANOFS:
			begin

			end
		READ_BYTE:
			begin
				up_word = 1'b1;
				o_nanofs_byte = 1'b1;

				if(counter_word_o == 32'h00000000)
					reg_sdspi_data_3_w = 1;
				else if (counter_word_o == 32'h00000001)
					reg_sdspi_data_2_w = 1;
				else if (counter_word_o == 32'h00000002)
					reg_sdspi_data_1_w = 1;
				else if (counter_word_o == 32'h00000003)
					reg_sdspi_data_0_w = 1;
				
			end
		PRE_LOAD:
			begin
				rst_counter_word = 1'b1;
			end
		LOAD_DATA_0:
			begin
				o_ram_we = 1'b1;
				o_ram_cyc = 1'b1;
				o_ram_stb = 1'b1;
				o_ram_sel = 4'b1111;
			end
		POST_LOAD:
			begin
				up = 1'b1;
			end
		END_FSM:
			begin
				finish_led = 1'b1;
				o_cpu_rst = 1'b0;
				o_mux = 1'b0;
			end
		ERROR:
			begin
				o_cpu_rst = 1'b0;
				o_mux = 1'b0;
			end
		default: 
			begin
			end
	endcase
end

//CS <= NS
always @(posedge clk)
begin
	if(reset)
		current_state <= IDLE;
	else if(i_nanofs_err)
		current_state <= ERROR;
	else
		current_state <= next_state;
end

endmodule

# Auto-generated project tcl file

create_project -part xc7a100tcsg324-1 nexys4ddr_rom_0

set_property "simulator_language" "Mixed" [current_project]



read_ip ../src/nexys4ddr_rom_0/rtl/verilog/design_1_axi_uartlite_0_0.xci
read_ip ../src/nexys4ddr_rom_0/rtl/verilog/MIG_ddr/ip/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.xci
read_ip ../src/nexys4ddr_rom_0/rtl/verilog/MIG_ddr/ip/design_1_mig_7series_0_0/design_1_mig_7series_0_0.xci
upgrade_ip [get_ips]
generate_target all [get_ips]


read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_wb_biu.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_or1k_status_reg.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_jsp_biu.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_or1k_module.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/syncflop.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_crc32.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_jsp_module.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_top.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/bytefifo.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_or1k_biu.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_or1k_defines.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/syncreg.v
read_verilog ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog/adbg_wb_module.v
read_verilog ../src/gpio_0/gpio.v
read_verilog ../src/jtag_tap_1.13/tap/rtl/verilog/tap_top.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_branch_prediction.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_bus_if_avalon.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_bus_if_wb32.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_cache_lru.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_cfgrs.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_cpu_cappuccino.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_cpu_espresso.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_cpu_prontoespresso.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_cpu.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_ctrl_cappuccino.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_ctrl_espresso.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_ctrl_prontoespresso.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_dcache.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_decode_execute_cappuccino.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_decode.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_dmmu.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_execute_alu.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_execute_ctrl_cappuccino.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_fetch_cappuccino.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_fetch_espresso.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_fetch_prontoespresso.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_fetch_tcm_prontoespresso.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_icache.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_immu.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_lsu_cappuccino.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_lsu_espresso.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_pic.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_rf_cappuccino.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_rf_espresso.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_simple_dpram_sclk.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_store_buffer.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_ticktimer.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_true_dpram_sclk.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_wb_mux_cappuccino.v
read_verilog ../src/mor1kx_3.1/rtl/verilog/mor1kx_wb_mux_espresso.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_alu.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_amultp2_32x32.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_cfgr.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_cpu.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_ctrl.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_dc_fsm.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_dc_ram.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_dc_tag.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_dc_top.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_dmmu_tlb.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_dmmu_top.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_dpram_256x32.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_dpram_32x32.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_dpram.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_du.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_except.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_addsub.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_arith.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_div.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_fcmp.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_intfloat_conv.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_intfloat_conv_except.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_mul.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_post_norm_addsub.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_post_norm_div.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_post_norm_intfloat_conv.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_post_norm_mul.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_pre_norm_addsub.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_pre_norm_div.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu_pre_norm_mul.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_fpu.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_freeze.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_genpc.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_gmultp2_32x32.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_ic_fsm.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_ic_ram.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_ic_tag.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_ic_top.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_if.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_immu_tlb.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_immu_top.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_iwb_biu.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_lsu.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_mem2reg.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_mult_mac.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_operandmuxes.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_pic.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_pm.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_qmem_top.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_reg2mem.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_rfram_generic.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_rf.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_sb_fifo.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_sb.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_1024x32_bw.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_1024x32.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_1024x8.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_128x32.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_2048x32_bw.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_2048x32.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_2048x8.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_256x21.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_32_bw.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_32x24.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_512x20.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_64x14.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_64x22.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram_64x24.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_spram.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_sprs.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_top.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_tpram_32x32.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_tt.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_wb_biu.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_wbmux.v
read_verilog ../src/or1200_0-r2/rtl/verilog/or1200_xcv_ram32x8d.v
read_verilog ../src/or1200_0-r2/rtl/verilog/timescale.v
read_verilog ../src/or1k_bootloaders_0.9.1/wb_bootrom.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/raminfr.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_debug_if.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_receiver.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_regs.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_rfifo.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_sync_flops.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_tfifo.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_top.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_transmitter.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_wb.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/timescale.v
read_verilog ../src/uart16550-1.5-vivado_0/rtl/verilog/uart_defines.v
read_verilog ../src/verilog-arbiter_0-r1/src/arbiter.v
read_verilog ../src/wb_intercon-vivado_1.0/rtl/verilog/wb_arbiter.v
read_verilog ../src/wb_intercon-vivado_1.0/rtl/verilog/wb_data_resize.v
read_verilog ../src/wb_intercon-vivado_1.0/rtl/verilog/wb_upsizer.v
read_verilog ../src/wb_intercon-vivado_1.0/rtl/verilog/wb_mux.v
read_verilog ../src/wb_intercon-vivado_1.0/rtl/verilog/verilog_utils.vh
read_verilog ../src/wb_intercon-vivado_1.0/rtl/verilog/wb_common.v
read_verilog ../src/wb_intercon-vivado_1.0/rtl/verilog/wb_common_params.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/switch.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/own_gpio.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/orpsoc_top.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/clkgen.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/rstgen.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/rom.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/wb_intercon.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/dvi_gen/convert_30to15_fifo.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/dvi_gen/dcmspi.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/dvi_gen/DRAM16XN.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/dvi_gen/dvi_encoder.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/dvi_gen/dvi_gen_top.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/dvi_gen/encode.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/dvi_gen/serdes_n_to_1.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/dvi_gen/synchro.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/contador_up.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/biestable_d.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/and_gate.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/pulse_button.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/bootloaderModule.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/registro.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/mux_3.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/counter_load.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/MIG_ddr/xilinx_ddr2.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/MIG_ddr/wb2axi.v
read_verilog ../src/nexys4ddr_rom_0/rtl/verilog/generic_mux.v



set_property include_dirs [list ../src/adv_debug_sys_0/Hardware/adv_dbg_if/rtl/verilog ../src/mor1kx_3.1/rtl/verilog ../src/or1k_bootloaders_0.9.1/ ../src/nexys4ddr_rom_0/rtl/verilog/include ../src/nexys4ddr_rom_0/rtl/verilog] [get_filesets sources_1]

read_xdc ../src/nexys4ddr_rom_0/data/Nexys4DDR_Master.xdc

set_property top orpsoc_top [current_fileset]

regexp -- {Vivado v([0-9]{4})\.[0-9]} [version] -> year

create_run -name synthesis -flow "Vivado Synthesis $year" -strategy "Vivado Synthesis Defaults"
create_run implementation -flow "Vivado Implementation $year" -strategy "Vivado Implementation Defaults" -parent_run synthesis

launch_runs implementation
wait_on_run implementation
open_run implementation
write_bitstream /home/yo/.config/fusesoc/build/nexys4ddr_rom_0/bld-vivado/nexys4ddr_rom_0.bit
